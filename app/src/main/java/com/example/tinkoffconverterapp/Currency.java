package com.example.tinkoffconverterapp;

import com.google.gson.annotations.SerializedName;

//class for getting list of currencies
public class Currency {
    @SerializedName("currencyName")
    private String currencyName;

    @SerializedName("currencySymbol")
    private String currencySymbol;

    @SerializedName("id")
    private String id;


    public Currency(String currencyName, String currencySymbol, String id) {
        this.currencyName = currencyName;
        this.currencySymbol = currencySymbol;
        this.id = id;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
