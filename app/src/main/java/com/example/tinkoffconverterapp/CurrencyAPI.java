package com.example.tinkoffconverterapp;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyAPI {

    @GET("currencies/")
    Call<JsonObject> loadCurrencies(@Query("apiKey") String apiKey);

    @GET("convert/")
    Call<Map<String, Double>> loadRatio(@Query("q") String fromTo, @Query("compact") String style, @Query("apiKey") String apiKey);
}
