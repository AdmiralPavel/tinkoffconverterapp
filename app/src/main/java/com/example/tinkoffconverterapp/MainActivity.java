package com.example.tinkoffconverterapp;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Important note: I made app only for 10 currencies due to API restrictions
public class MainActivity extends AppCompatActivity {

    private static final String API_KEY = "ae9c2e11ce112b2fa4d3";
    private static final String COMPACT = "ultra";
    private static Map<String, Double> USD_TO_SMTH;
    private static CurrencyAPI currencyAPI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //some views to work with
        final EditText convertFromEditText = findViewById(R.id.convertFromEditText);
        final EditText convertToEditText = findViewById(R.id.convertToEditText);
        Spinner fromSpinner = findViewById(R.id.spinnerFrom);
        Spinner toSpinner = findViewById(R.id.spinnerTo);


        //set listener for refresher
        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            initialize();
            swipeRefreshLayout.setRefreshing(false);
        });

        //listeners for text changing
        convertFromEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (getCurrentFocus() == convertFromEditText && fromSpinner.getSelectedItem() != null) {
                    String firstCurrency = fromSpinner.getSelectedItem().toString();
                    String secondCurrency = toSpinner.getSelectedItem().toString();
                    double firstValue = 0;

                    if (!convertFromEditText.getText().toString().equals("")) {
                        try {
                            firstValue = Double.parseDouble(convertFromEditText.getText().toString());

                            if (!firstCurrency.equals("") && !secondCurrency.equals("")) {
                                if (firstCurrency.equals("USD"))
                                    convertToEditText.setText(String.format("%.2f", USD_TO_SMTH.get(firstCurrency + "_" + secondCurrency) * firstValue));
                                else
                                    convertToEditText.setText(String.format("%.2f", firstValue / USD_TO_SMTH.get("USD_" + firstCurrency) * USD_TO_SMTH.get("USD_" + secondCurrency)));
                            }
                        } catch (NumberFormatException e) {
                        }
                    } else {
                        convertToEditText.setText("");
                    }
                }
            }
        });

        convertToEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (getCurrentFocus() == convertToEditText && fromSpinner.getSelectedItem() != null) {
                    Spinner fromSpinner = findViewById(R.id.spinnerFrom);
                    Spinner toSpinner = findViewById(R.id.spinnerTo);


                    String firstCurrency = toSpinner.getSelectedItem().toString();
                    String secondCurrency = fromSpinner.getSelectedItem().toString();
                    double firstValue = 0;
                    if (!convertToEditText.getText().toString().equals("")) {
                        try {
                            firstValue = Double.parseDouble(convertToEditText.getText().toString());

                            if (!firstCurrency.equals("") && !secondCurrency.equals("")) {
                                if (firstCurrency.equals("USD"))
                                    convertFromEditText.setText(String.format("%.2f", USD_TO_SMTH.get(firstCurrency + "_" + secondCurrency) * firstValue));
                                else
                                    convertFromEditText.setText(String.format("%.2f", firstValue / USD_TO_SMTH.get("USD_" + firstCurrency) * USD_TO_SMTH.get("USD_" + secondCurrency)));
                            }

                        } catch (NumberFormatException e) {
                        }
                    } else
                        convertFromEditText.setText("");
                }
            }
        });

        //initializing of views
        initialize();

    }

    //method for making HTTP query to the server
    private Callback<JsonObject> callbackCurrenciesList = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            if (response.isSuccessful()) {

                //parsing downloaded data for different currencies
                Map<String, Currency> list = response.body().getList();
                List<String> data = new ArrayList<>();
                int i = 0;
                for (Map.Entry<String, Currency> cur : list.entrySet()) {
                    i++;
                    data.add(cur.getKey());
                    if (i >= 10) break;
                }

                //initializing spinners with adapters
                Spinner spinnerFrom = findViewById(R.id.spinnerFrom);
                Spinner spinnerTo = findViewById(R.id.spinnerTo);

                initSpinner(spinnerFrom, spinnerTo, data, findViewById(R.id.convertFromEditText), findViewById(R.id.convertToEditText));
                initSpinner(spinnerTo, spinnerFrom, data, findViewById(R.id.convertToEditText), findViewById(R.id.convertFromEditText));

                //creating view for time information
                TextView infoTextView = findViewById(R.id.infoTextView);
                Date currentTime = Calendar.getInstance().getTime();
                String str = "Данные актуальны на " + currentTime.toString();
                infoTextView.setText(str);

                String joined = "USD_" + TextUtils.join(",USD_", data);
                Call<Map<String, Double>> requestRatio = currencyAPI.loadRatio(joined, COMPACT, API_KEY);
                requestRatio.enqueue(callbackCurrenciesRatio);

            } else {
                //in bad cases
                Toast toast = Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.message, Toast.LENGTH_SHORT);
            toast.show();
        }
    };

    private Callback<Map<String, Double>> callbackCurrenciesRatio = new Callback<Map<String, Double>>() {
        @Override
        public void onResponse(Call<Map<String, Double>> call, Response<Map<String, Double>> response) {
            USD_TO_SMTH = response.body();
        }

        @Override
        public void onFailure(Call<Map<String, Double>> call, Throwable t) {
            Toast toast = Toast.makeText(getApplicationContext(), "Пожалуйста, проверьте соединение с интернетом", Toast.LENGTH_SHORT);
            toast.show();
        }
    };

    //method for initializing spinners with adapters
    void initSpinner(Spinner fromSpinner, Spinner toSpinner, List<String> data, EditText convertFromEditText, EditText convertToEditText) {
        ArrayAdapter<String> spinnerListAdapter = new ArrayAdapter<>
                (getApplicationContext(), android.R.layout.simple_spinner_item,
                        data);
        spinnerListAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        fromSpinner.setAdapter(spinnerListAdapter);

        //click listener for making EditText changing when changing currency
        fromSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (fromSpinner.getSelectedItem() != null) {
                    String firstCurrency = toSpinner.getSelectedItem().toString();
                    String secondCurrency = fromSpinner.getSelectedItem().toString();
                    double firstValue = 0;
                    if (!convertToEditText.getText().toString().equals("")) {
                        try {
                            firstValue = Double.parseDouble(convertToEditText.getText().toString());

                            if (!firstCurrency.equals("") && !secondCurrency.equals("")) {
                                if (firstCurrency.equals("USD"))
                                    convertFromEditText.setText(String.format("%.2f", USD_TO_SMTH.get(firstCurrency + "_" + secondCurrency) * firstValue));
                                else
                                    convertFromEditText.setText(String.format("%.2f", firstValue / USD_TO_SMTH.get("USD_" + firstCurrency) * USD_TO_SMTH.get("USD_" + secondCurrency)));
                            }

                        } catch (NumberFormatException e) {
                        }
                    } else
                        convertFromEditText.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    //method for initializing application (checking connection, sending requests and other)
    void initialize() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            currencyAPI = RetrofitInstance.getRetrofit().create(CurrencyAPI.class);
            Call<JsonObject> requestList = currencyAPI.loadCurrencies(API_KEY);
            requestList.enqueue(callbackCurrenciesList);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Пожалуйста, проверьте соединение с интернетом", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}