package com.example.tinkoffconverterapp;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class JsonObject {
    @SerializedName("results")
    private Map<String,Currency> list;

    public JsonObject(Map<String,Currency> list) {
        this.list = list;
    }

    public Map<String,Currency> getList() {
        return list;
    }

    public void setList(Map<String,Currency> list) {
        this.list = list;
    }
}
